/*$(document).ready(function(){
	alert("Huomenta jQuery World!");		
	console.log("jquery/javascript toimii");	
});*/

//Kuuntelee id="Musta" nappia, vaihtaa taustan värin mustaksi ja tekstin valkoiseksi.
$( "#Musta" ).click(function() {
    $('#tausta').css('background-color', 'Black');
    $('#Otsikko').css('color', 'White');
    $('#joonas').css('color', 'White');
  });
//Kuuntelee id="Harmaa" nappia, vaihtaa taustan värin harmaaksi ja tekstin mustaksi.
  $( "#Harmaa" ).click(function() {
    $('#tausta').css('background-color', 'Gray');
    $('#Otsikko').css('color', 'Black');
    $('#joonas').css('color', 'Black');
  });
//Kuuntelee id="Valkoinen" nappia, vaihtaa taustan värin valkoiseksi ja tekstin mustaksi.
  $( "#Valkoinen" ).click(function() {
    $('#tausta').css('background-color', 'White');
    $('#Otsikko').css('color', 'Black');
    $('#joonas').css('color', 'Black');
  });
